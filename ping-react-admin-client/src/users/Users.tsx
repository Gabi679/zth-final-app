import React from 'react';
import { User } from '../models/User';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import axios, { AxiosResponse } from 'axios';
import { Button } from 'primereact/button';
import io from 'socket.io-client';

class UsersComponent extends React.Component<{ users: User[], ready: Map<string, boolean> }> {
    socket: any;
    state = {users: [],  ready: new Map<string, boolean>()};
    componentDidMount() {
        this.socket = io(`${process.env.REACT_APP_WEBSOCKET_URL}`);
        axios.get(`${process.env.REACT_APP_API_URL}/getUsers`).then((response: AxiosResponse<User[]>) => {
          this.setState({ users: response.data });
        });
      }

    test() {
        this.socket.emit('test');
        this.socket.on('testResult', (result: string) => {
            console.log(result)
            const ready = new  Map<string, boolean>();
            JSON.parse(result).forEach((element:any) => {
                ready.set(element[0], element[1]);
            });
            this.setState({users: this.state.users, ready})
          });
    }

    deleteUser(ip: string){
        axios.post(`${process.env.REACT_APP_API_URL}/removeUser`, {ip}).then((response: AxiosResponse<User[]>) => {
            this.setState({ users: response.data });
          });
    }

    computerNumberTemplate(rowData: any, column: any) {
        const background = this.state.ready.get(rowData['ip']) === true ? '#83ff0033' : this.state.ready.get(rowData['ip']) === false ?  '#ff000033' : 'transparent';
        return <div style={{background}}>{rowData['computerNumber']}</div>;
    }

    ipTemplate(rowData: any, column: any) {
        const background = this.state.ready.get(rowData['ip']) === true ? '#83ff0033' : this.state.ready.get(rowData['ip']) === false ?  '#ff000033' : 'transparent';
        return <div style={{background}}>{rowData['ip']}</div>;
    }

    delete(rowData: any, column: any) {
        const ip = rowData['ip'];
        return <Button  onClick={() => this.deleteUser(ip)} label="Delete" ></Button>;
    }

    userNameTemplate(rowData: any, column: any) {
        const background = this.state.ready.get(rowData['ip']) === true ? '#83ff0033' : this.state.ready.get(rowData['ip']) === false ?  '#ff000033' : 'transparent';
        return <div style={{background}}>{rowData['userName']}</div>;
    }

    render() {
        return (
            <div>
                <Button label="Test user connection" onClick={() => this.test()} />
                <DataTable value={this.state.users}>
                    <Column field="ip" header="IP"   body={this.ipTemplate.bind(this)}/>
                    <Column field="userName" header="Name"  body={this.userNameTemplate.bind(this)} />
                    <Column field="computerNumber" header="computer Number"  body={this.computerNumberTemplate.bind(this)} />
                    <Column field="ip"  header="delete"  body={this.delete.bind(this)} />
                </DataTable>
            </div>

        );
    }
}

export default UsersComponent;
