import React, { Component, Children } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { User } from '../models/User';
import { Exercise } from '../models/Exercise';
import './resultTable.css'
import { Result } from '../models/Result';
import io from 'socket.io-client';

interface DataSet {
  data: {
    user: User | undefined,
    result: number
  };
  children: Exercise[];
}


export class ResultTable extends Component<any, { expandedRows: DataSet[], nodes: DataSet[] }> {
  nodes: DataSet[] = [];

  constructor(props: { results: Result[] }) {
    super(props);
    this.state = {
      nodes: [],
      expandedRows: []
    };
  }

  socket: any;
  componentDidMount() {
    this.socket = io(`${process.env.REACT_APP_WEBSOCKET_URL}`);
    this.socket.on('result', (result: Result[]) => {
      this.setState({nodes: this.getTableStructure(result)});
    });
  }


  private getTableStructure(resultList: Result[]): DataSet[] {
    return resultList.map(result => {
      return {
        data: {
          user: result.user,
          result: result.exercises.map(exercise=>exercise.result).reduce((a, b) => a + b, 0)
        },
        children: result.exercises
      }
    });

}

rowExpansionTemplate(data: DataSet) {
  return (
    <DataTable value={data.children} className="innerTable">
      <Column field="id" header="Exercise name"></Column>
      <Column field="result" header="Points"></Column>
    </DataTable>
  );
}

render() {
  return (

    <DataTable value={this.state.nodes} expandedRows={this.state.expandedRows} onRowToggle={(e) => this.setState({ expandedRows: e.data })}
      rowExpansionTemplate={this.rowExpansionTemplate} dataKey="data.user.ip">
      <Column expander style={{ width: '3em' }} />
      <Column field="data.user.userName" header="UserName"></Column>
      <Column field="data.result" header="Total points"></Column>
    </DataTable>
  );
}
}