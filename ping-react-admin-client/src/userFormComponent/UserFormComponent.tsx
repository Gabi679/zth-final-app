import React from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Card } from 'primereact/card';
import axios, { AxiosResponse } from 'axios';
import { User } from '../models/User';
import { Messages } from 'primereact/messages';
import { Message } from 'primereact/message';

class UserFormComponent extends React.Component<{ onSave: Function }, { ip: string, userName: string, computerNumber: string, formError: boolean }> {

    state = { ip: '', userName: '', computerNumber: '', formError: true };
    messages: any;
    save() {
        axios.post(`${process.env.REACT_APP_API_URL}/addUser`, this.state).then((response: AxiosResponse<User[]>) => {
            this.messages.show({ severity: 'success', summary: 'Success', detail: 'Sikeres felhasználó hozzáadás!' });
            this.setState({ ip: '', userName: '', computerNumber: '', formError: true });
        }).catch(e => {
            this.messages.show({ severity: 'error', summary: 'Error', detail: e.message });
        });
    }

    render() {
        return (
            <div>
                <Card title="Registration" style={{ width: "fit-content", margin: "2em auto 0 auto" }}>
                    <div>
                        <div className="p-field p-grid">
                            <label htmlFor="firstname3" className="p-col-fixed" style={{ width: '100px' }}>Username</label>
                            <div className="p-col">
                                <InputText id="name" placeholder="example user" value={this.state.userName} onChange={e => this.setState({ userName: e.currentTarget.value, formError: (e.currentTarget.value==='' || this.state.ip==='' || this.state.computerNumber==='') })} />
                            </div>
                        </div>
                        <div className="p-field p-grid">
                            <label htmlFor="lastname3" className="p-col-fixed" style={{ width: '100px' }}>IP. address</label>
                            <div className="p-col">
                                <InputText id="ip" placeholder="192.168.2.1" value={this.state.ip} onChange={e => this.setState({ ip: e.currentTarget.value,  formError: (e.currentTarget.value==='' || this.state.userName==='' || this.state.computerNumber==='')  })} />
                            </div>
                        </div>
                        <div className="p-field p-grid">
                            <label htmlFor="lastname3" className="p-col-fixed" style={{ width: '100px' }}>Computer number</label>
                            <div className="p-col">
                                <InputText id="computer number"  value={this.state.computerNumber} onChange={e => this.setState({ computerNumber: e.currentTarget.value,  formError: (e.currentTarget.value==='' || this.state.ip==='' || this.state.userName==='')  })} />
                            </div>
                        </div>
                        <Button label="Save" disabled={this.state.formError} onClick={() => this.save()} />
                    </div>
                </Card>
                <Messages ref={(el) => this.messages = el}></Messages>
            </div>
        );
    }
}

export default UserFormComponent;
