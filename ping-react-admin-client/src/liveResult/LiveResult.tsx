import React from 'react';
import { Chart } from 'chart.js';
import io from 'socket.io-client';
import { Result } from '../models/Result';

class LiveResultComponent extends React.Component<any, {chart: Chart}> {
    socket: any;
    componentDidMount() {
        this.socket = io(`${process.env.REACT_APP_WEBSOCKET_URL}`);
        this.socket.on('result', (result: Result[]) => {
          this.state.chart.data.labels = [];
          if(this.state.chart.data.datasets && this.state.chart.data.datasets[0].data){
            this.state.chart.data.datasets[0].data = [];
          }
          this.getChartStructure(result).forEach(data=>{
              if(this.state.chart.data.labels){
                this.state.chart.data.labels.push(data.userName);
              }
              if(this.state.chart.data.datasets && this.state.chart.data.datasets[0].data){
                this.state.chart.data.datasets[0].data.push(data.result);
              }
             
          });
          this.state.chart.update();
        });
        const ctx = document.getElementById('myChart');
        if (ctx != null) {
            const chart = new Chart(ctx as HTMLCanvasElement, {
                type: 'horizontalBar',
                data: {
                    labels: [],
                    datasets: [{
                        label: '# of Points',
                        data: [],
                        backgroundColor: ["#388dbb", "#5ebe68", "#2650b9", "#d47f24", "#718bd1", "#b34aa9", "#d6f496", "#682392", "#8f3532", "#281b4f"],
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            stacked: true,
                            ticks: {
                                beginAtZero: true,
                                suggestedMin: 0
                            }
                        }],
                        xAxes: [{
                            stacked: true,
                            ticks: {
                                beginAtZero: true,
                                suggestedMin: 0
                            }
                        }]
                    }
                }
            });
            this.setState({chart});
        }
    }

    private getChartStructure(resultList: Result[]): {userName: string, result: number}[] {
        return resultList.map(result => {
          return {
            userName: result.user.userName,
            result: result.exercises.map(exercise=>exercise.result).reduce((a, b) => a + b, 0)
          }
        });
    }


    render() {
        return (
            <canvas id="myChart" ></canvas>
        );
    }
}

export default LiveResultComponent;
