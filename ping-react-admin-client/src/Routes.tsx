import React from 'react';
import './App.css';
import UserFormComponent from './userFormComponent/UserFormComponent';
import { ResultTable } from './resultTable/ResultTable';
import UsersComponent from './users/Users';
import { Switch, Route } from 'react-router-dom';
import LiveResultComponent from './liveResult/LiveResult';
import AdminComponent from './admin/Admin';


class Routes extends React.Component {

  state = { users: [] };

  render() {
    return (
        <Switch>
          <Route path="/" exact component={UserFormComponent} />
          <Route path="/users" exact component={UsersComponent} />
          <Route path="/results" exact component={ResultTable} />
          <Route path="/live-result" exact component={LiveResultComponent} />
          <Route path="/admin" exact component={AdminComponent} />
        </Switch>
    );
  }
}

export default Routes;
