import React from 'react';
import io from 'socket.io-client';
import { Button } from 'primereact/button';
import axios, { AxiosResponse } from 'axios';
import { Messages } from 'primereact/messages';

class AdminComponent extends React.Component<any, { exercises: { id: string, isRunning: boolean }[] }> {
    socket: any;
    messages: any;
    state = { exercises: [] }
    componentDidMount() {
        this.socket = io(`${process.env.REACT_APP_WEBSOCKET_URL}`);
        axios.get(`${process.env.REACT_APP_API_URL}/getExercises`).then((response: AxiosResponse<{ id: string, isRunning: boolean }[]>) => {
            this.setState({ exercises: response.data });
        });
    }


    persist() {
        axios.get(`${process.env.REACT_APP_API_URL}/persist`).then((response: AxiosResponse<string>) => {
            this.messages.show({ severity: 'success', summary: 'Success', detail: 'Sikeresen mentés' });
        }).catch(e=>{
            this.messages.show({ severity: 'Error', summary: 'Error', detail: 'Sikertelen mentés' });
        });
    }

    startExercise(id: string) {
        this.setState({
            exercises: this.state.exercises.map((exercise: { id: string, isRunning: boolean }) => {
                if (exercise.id === id) {
                    exercise.isRunning = true;
                }
                return exercise;
            })
        })
        this.socket.emit('start', id);
    }

    stopExercise(id: string) {
        this.setState({
            exercises: this.state.exercises.map((exercise: { id: string, isRunning: boolean }) => {
                if (exercise.id === id) {
                    exercise.isRunning = false;
                }
                return exercise;
            })
        })
        this.socket.emit('stop', id);
    }

    stopAllExercise() {
        this.setState({
            exercises: this.state.exercises.map((exercise: { id: string, isRunning: boolean }) => {
                exercise.isRunning = false;
                return exercise;
            })
        })
        this.socket.emit('stopAll');
    }

    render() {
        return (
            <div>
                <table>
                    <tbody>
                        <tr>
                            <td colSpan={3}>
                                <Button label="Persist data" style={{ margin: '0 auto 0 auto', display: 'block' }}  onClick={() => this.persist()} />
                            </td>
                        </tr>
                        <tr>
                            <td colSpan={3}>
                                <Button label="Stop All" style={{ margin: '0 auto 0 auto', display: 'block' }}  onClick={() => this.stopAllExercise()} />
                            </td>
                        </tr>
                        {
                            this.state.exercises.map((exercise: { id: string, isRunning: boolean }) => {
                                return <tr  >

                                    <td >{exercise.id}</td>
                                    <td  ><Button label="Start" disabled={exercise.isRunning} onClick={() => this.startExercise(exercise.id)} /></td>
                                    <td  ><Button label="Stop" disabled={!exercise.isRunning} onClick={() => this.stopExercise(exercise.id)} /></td>
                                </tr>
                            })
                        }
                    </tbody>
                </table>
                <Messages ref={(el) => this.messages = el}></Messages>
            </div>);
    }
}

export default AdminComponent;
