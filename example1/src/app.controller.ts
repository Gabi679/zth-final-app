import { Controller, Get, Put, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get('first')
  getFirst(): string {
    console.log("first accepted");
    return this.appService.getFirst();
  }

  @Get('try')
  try() {
    const graph = this.createGraph(new GraphNode(this.getRandomInt(100)), this.getRandomInt(3) + 2);

    const houses = this.getArray([graph]);
    const res = this.solve(houses);
    return res;
  }
  @Post('sumOfPowers')
  sumOfPowers(@Body('n') n: number, @Body('x') x: number): number {
    console.log("asd")
    return this.combinations(x, n, 1);
  }

  combinations(X, N, c) {
    if (X === 0) {
      return 1;
    }
    var result = 0;
    for (var i = c; Math.pow(i, N) <= X; i++) {
      result += this.combinations(X - Math.pow(i, N), N, i + 1);
    }
    return result;
  }

  getArray(nodes: GraphNode[], array = []) {
    const currentNode = nodes.pop();
    array.push(currentNode ? currentNode.value : null);
    if (currentNode && currentNode.value) {
      nodes = [currentNode.right, currentNode.left, ...nodes];
    }
    if (nodes.length) {
      this.getArray(nodes, array);
    }
    return array;
  }

  getRandomInt(max): number {
    return Math.floor(Math.random() * Math.floor(max)) + 1;
  }

  createGraph(root: GraphNode, max: number) {
    if (root && root.value !== null && max) {
      root.left = this.createGraph(new GraphNode(this.getRandomInt(100)), max - 1)
      root.right = this.createGraph(new GraphNode(this.getRandomInt(100)), max - 1)
    }
    return root;
  }

  @Get('second')
  getSecond(): string {
    return this.appService.getSecond();
  }

  @Post('fixPoint')
  fixPoint(): string {
    return Math.random() >= 0.1 ? 'good' : 'bad';
  }


  @Post('maxSubArray')
  maxSubArray(@Body('inputArray') input: number[]): number[] {
    console.log(input, this.appService.maxSubArraySolver(input))
    return this.appService.maxSubArraySolver(input);
  }  
  
  @Post('flood')
  flood(@Body('floodData') floodMap: number[]): number[] {
    let floodMapDiffMask = []
    for (let index = 0; index < floodMap.length - 1; index++) {
        floodMapDiffMask.push(floodMap[index + 1] - floodMap[index] > 2 ? 1 : 0);
    }
    let index = floodMapDiffMask.length - 1;

    let floodBegin = -1000;
    let floodEnd = -1000;
    let count = 0;
    let sequenceFound = false;

    while(index >= 0 && (floodBegin === -1000 || floodEnd === -1000)) {
        if(floodMapDiffMask[index] === 1 && !sequenceFound) {
            count++;
            floodEnd = index;
            sequenceFound = true;
        } else if (floodMapDiffMask[index] === 1 && sequenceFound) {
            count++;
        } else if (floodMapDiffMask[index] === 0 && sequenceFound) {
            sequenceFound = false;
            if (count > 3) {
                floodBegin = index;
            } else {
                floodEnd = -1000;
            }
            count = 0;
        }
        index--;
    }
    if(floodEnd !== -1000) {
        floodEnd++;
    }
    if(floodBegin !== -1000) {
        floodBegin = floodBegin + 2;
    }
    return [floodBegin, floodEnd];
  }


  @Get('ping')
  getPing(): string {
    console.log("ping")
    return "pong";
  }

  @Post('colorMixing')
  colorMixing(@Body('exercise') exercise: string, @Body('colors') colors: string[]): string {
    console.log(colors, this.appService.getColor(colors))
    return this.appService.getColor(colors);
  }


  @Post('set-first')
  setFirst(@Body() body: { goodAnswer: boolean }) {
    this.appService.setFirst(body.goodAnswer);
  }

  @Post('set-second')
  setSecond(@Body() body: { goodAnswer: boolean }) {
    this.appService.setSecond(body.goodAnswer);
  }

  @Post('robbery')
  robbery(@Body('houses') houses: number[]) {
    return this.solve(houses);
  }

  solve(houses) {
    let nullCounter = 0;
    let deep = 0;
    const root = this.insertLevelOrder(houses, new GraphNode(houses[0]), 0);
    let max = this.calculate(root);
    if (houses[1]) {
      const leftMax = this.calculate(root.left);

      if (leftMax > max) {
        max = leftMax;
      }
    }
    if (houses[2]) {
      const rightMax = this.calculate(root.right);

      if (rightMax > max) {
        max = rightMax;
      }
    }
    return max;
  }

  calculate(root, money = 0) {
    money += root.value;
    let maxMoney = 0;
    if (root.left && root.left.value && root.left.left && root.left.left.value) {
      let currentMoney = this.calculate(root.left.left);
      currentMoney > maxMoney ? maxMoney = currentMoney : null;
    }
    if (root.left && root.left.right && root.left.value && root.left.right.value) {
      let currentMoney = this.calculate(root.left.right);
      currentMoney > maxMoney ? maxMoney = currentMoney : null;
    }
    if (root.right && root.right.right && root.right.value && root.right.right.value) {
      let currentMoney = this.calculate(root.right.right);
      currentMoney > maxMoney ? maxMoney = currentMoney : null;
    }
    if (root.right && root.right.left && root.right.value && root.right.left.value) {
      let currentMoney = this.calculate(root.right.left);
      currentMoney > maxMoney ? maxMoney = currentMoney : null;
    }
    return money + maxMoney;
  }

  insertLevelOrder(arr: number[], root: GraphNode, i: number): GraphNode {
    // Base case for recursion
    if (i < arr.length) {
      const temp: GraphNode = new GraphNode(arr[i]);
      root = temp;

      // insert left child
      root.left = this.insertLevelOrder(arr, root.left,
        2 * i + 1);

      // insert right child
      root.right = this.insertLevelOrder(arr, root.right,
        2 * i + 2);
    }
    return root;
  }








}

class GraphNode {
  value = null;
  left = null;
  right = null;

  constructor(value) {
    this.value = value;
  }
}
