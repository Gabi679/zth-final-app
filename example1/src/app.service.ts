import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {

  isGoodFirst: boolean = false;
  isGoodSecond: boolean = false;

  getFirst(): string {
    if (this.isGoodFirst) {
      return "good";
    }
    return "bad";
  }
  getSecond(): string {
    if (this.isGoodSecond) {
      return "good";
    }
    return "bad";
  }

  setFirst(goodAnswer: boolean) {
    this.isGoodFirst = goodAnswer;
  }
  setSecond(goodAnswer: boolean) {
    this.isGoodSecond = goodAnswer;
  }
  getHello(): string {
    return 'Hello World!';
  }

  getColor(colors: string[]): string {
    if (colors.length === 1) {
      return colors[0];
    } else {
      const red = colors.includes("Red");
      const green = colors.includes("Green");
      const blue = colors.includes("Blue");
      const magenta = colors.includes("Magenta");
      const yellow = colors.includes("Yellow");
      const cyan = colors.includes("Cyan");
      if (red && green && blue) {
        return "White";
      } else if (magenta && yellow && cyan) {
        return "Black";
      } else if (red && blue) {
        return "Magenta";
      } else if (red && green) {
        return "Yellow";
      } else if (blue && green) {
        return "Cyan";
      } else if ((magenta && yellow)) {
        return "Red";
      } else if ((magenta && cyan)) {
        return "Blue";
      } else if ((cyan && yellow)) {
        return "Green";
      } else if (yellow) {
        return "Yellow";
      } else if (red) {
        return "Red";
      } else if (blue) {
        return "Blue";
      } else if (magenta) {
        return "Magenta";
      } else if (cyan) {
        return "Cyan";
      }
      return "White";
    }
  }

  maxSubArraySolver(inputArray: number[] ) : number[] {
    let maxContigSum = 0;
    let maxNonContigSum = 0;
    let currentContigSum = 0;
    let largestValue = 0;

    let resultArray : number[] = [];

    inputArray.forEach(element => {
        // get the maxContigSum
        let value = currentContigSum + element;

        if(value > 0) {
            currentContigSum = value;
        } else {
            currentContigSum = 0;
        }
        //set if grater then the current max
        if(currentContigSum > maxContigSum) {
            maxContigSum = currentContigSum;
        }
        //get the max nonContingSum
        if(element > 0) {
            maxNonContigSum += element;
        }
        // keep largest value
        if(!largestValue || element > largestValue) {
            largestValue = element
        }
    });

    if(maxNonContigSum !== 0) {
        resultArray.push(maxContigSum, maxNonContigSum);
    } else {
        resultArray.push(largestValue, largestValue);
    }

    return resultArray;
  }
}
