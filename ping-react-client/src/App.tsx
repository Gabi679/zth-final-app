import React, { Component } from 'react';
import { Link, BrowserRouter } from 'react-router-dom';
import history from './services/history';
import Routes from './Routes';
import { Toolbar } from 'primereact/toolbar';
import { Button } from 'primereact/button';

export class App extends Component {
  render() {
    return (
      <BrowserRouter >
        <Toolbar>
          <div className="p-toolbar-group-left">
            <Link to="/results">
              <Button label="Results" icon="pi pi-check" style={{ marginRight: '.25em' }} />
            </Link>
            <Link to="/live-result">
              <Button label="Live results" icon="pi pi-chart-bar" style={{ marginRight: '.25em' }} />
            </Link>
          </div>
        </Toolbar>
        <Routes />
      </BrowserRouter>
    );
  }
}
export default App;