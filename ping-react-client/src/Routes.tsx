import React from 'react';
import './App.css';
import { ResultTable } from './resultTable/ResultTable';
import { Switch, Route } from 'react-router-dom';
import LiveResultComponent from './liveResult/LiveResult';


class Routes extends React.Component {

  state = { users: [] };

  render() {
    return (
        <Switch>
          <Route path="/" exact component={ResultTable} />
          <Route path="/results" exact component={ResultTable} />
          <Route path="/live-result" exact component={LiveResultComponent} />
        </Switch>
    );
  }
}

export default Routes;
