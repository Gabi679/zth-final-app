import { Injectable } from '@nestjs/common';

@Injectable()
export class ExerciseService {
    private initialized = false;
    private runningExercises: string[] = [];
    private pausedExercises: string[] = [];

    addRunningExercise(exercise: string) {
        if (!this.runningExercises.includes(exercise)) {
            this.runningExercises.push(exercise);
        }else if(this.pausedExercises.includes(exercise)){
            this.pausedExercises = this.pausedExercises.filter(exe => exe !== exercise);
        }
    }

    removeRunningExercise(exercise: string) {
        if (this.runningExercises.includes(exercise)) {
            this.runningExercises = this.runningExercises.filter(exe => exe !== exercise);
        }
    }

    stopAllExercise() {
        this.runningExercises = [];
    }

    pauseExercise(exercise: string) {
        if (!this.pausedExercises.includes(exercise)) {
            this.pausedExercises.push(exercise);
        }
    }

    pauseAllExercise() {
       this.runningExercises.forEach(this.pauseExercise.bind(this));
    }

    isExerciseRunning(exercise: string): boolean {
        return this.runningExercises.includes(exercise) && !this.pausedExercises.includes(exercise) ;
    }

    isThereAnyRunningExercise(): boolean {
        return this.runningExercises.length!==0 && this.pausedExercises.length===0;
    }

    getRunningExercises(): string[] {
        return this.runningExercises;
    }
}
