import { Injectable } from '@nestjs/common';
import { User } from 'src/models/User';
import { AppGateway } from 'src/app.gateway';
import { uuid } from 'uuidv4';
import {EventEmitter} from 'events'
import { BehaviorSubject } from 'rxjs';
import { Result } from 'src/models/Result';

@Injectable()
export class UserService {
    public newUserSubject = new BehaviorSubject<User>(null);
    public removeUserSubject = new BehaviorSubject<User>(null);

    users: User[] = [
    ];

    addUser(user: User): User[] {
        this.newUserSubject.next(user)
        this.users.push(user);
        return this.users;
    }

    getUsers(): User[] {
        return this.users;
    }  
    
    removeUser(ip: string): User[] {
        this.users = this.users.filter(user=>{
            if(user.ip !== ip){
                return true;
            }
            this.removeUserSubject.next(user);
            return false;
        });
        return this.users;
    }


}
