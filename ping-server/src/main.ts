import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import localStorage  from './utils/LocalStorage';


async function bootstrap() {
  localStorage.clear();
  const app = await NestFactory.create(AppModule, { cors: { origin: "*", credentials: true } });
  await app.listen(3002, '0.0.0.0');

}
bootstrap();
