

const fs = require('fs');

process.on('message', async (result) => {
    if(result.length){
        let stringToWrite = 'name,ip,computernumber,'+result[0].exercises.map(exer=>exer.id).join()+",sum\n";
        result.forEach(userObj=>{
            stringToWrite+=`${userObj.user.userName},${userObj.user.ip},${userObj.user.computerNumber}`;
            let sum = 0;
            userObj.exercises.forEach(exercise=>{
                sum+=exercise.result;
                stringToWrite+=`,${exercise.result}`
            });
            stringToWrite+=`,${sum}\n`;
        });
        fs.writeFileSync('test.csv', stringToWrite);
    }
    process.send({ result: 'Process ended' });
});