export interface Exercise {
    id: string;
    result: number;
}
