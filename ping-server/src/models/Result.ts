import { Exercise } from "./Exercise";
import { User } from "./User";

export interface Result {
    id: string;
    exercises: Exercise[];
    user: User;
}