import { Controller, Get, Body, Post } from "@nestjs/common";
import { registeredFunctions } from "./decorators/Exercise";
import { User } from "./models/User";
import { UserService } from "./services/user/user.service";
import { ExerciseService } from "./services/exercise/exercise.service";
import fork = require('child_process');
import { AppGateway } from "./app.gateway";

@Controller()
export class AppController {

  constructor(private userService: UserService, private exerciseService:ExerciseService, private gateway:AppGateway) {}

  @Post("/addUser")
  addUser(@Body() user: User): User[] {
    return this.userService.addUser(user);
  }

  @Get("/getUsers")
  getUsers(): User[] {
    return this.userService.getUsers();
  }

  @Post("/removeUser")
  removeUser(@Body('ip') ip: string): User[] {
    return this.userService.removeUser(ip);
  }

  @Get("/getExercises")
  getExercises(): {id: string, isRunning: boolean}[] {
    return Array.from(registeredFunctions.keys()).filter(key=>key!='test').map(exercise=> {
      return {id: exercise, isRunning: this.exerciseService.isExerciseRunning(exercise)}
    });
  }


  @Get("/persist")
  async persist(): Promise<string> {
    let promise = new Promise<string>(
        (resolve, reject) => {
          const process = fork.fork('src/persist.ts');
          process.on('message', (message: { result: string}) => {
            resolve( message.result);
          });
          process.send(this.gateway.resultList);    
        }
    );   
    return await promise;
  }
}