export const registeredFunctions = new Map<string, Function>();
export function Exercise(id: string) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        registeredFunctions.set(id, target.mainExercise.bind(target));
    };
}