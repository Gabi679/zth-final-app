export abstract class BaseExercise {
    abstract async mainExercise(url: string, callback)
}