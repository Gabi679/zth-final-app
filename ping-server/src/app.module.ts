import { Module, HttpModule } from '@nestjs/common';
import { AppGateway } from './app.gateway';
import { AppController } from './app.controller';
import { ColorMixingExercise } from './exercises/ColorMixingExercise';
import { UserService } from './services/user/user.service';
import { ExerciseService } from './services/exercise/exercise.service';
import { CalculationExercise } from './exercises/CalculationExercise';
import { TestExercise } from './exercises/Test';
import { MaxExercise } from './exercises/MaxExercise';
import { CoordinatesExercise } from './exercises/Coordinates';
import { SumOfPowersExercise } from './exercises/SumOfPowersExercise';
import { RobberyExercise } from './exercises/RobberyExercise';
import { MaxSubArrayExercise } from './exercises/MaxSubArrayExercise';
import { FloodExercise } from './exercises/FloodExercise';


@Module({
    imports: [HttpModule],
    controllers: [AppController],
    providers: [
        AppGateway,
        UserService,
        ExerciseService,
        TestExercise,
        ColorMixingExercise,
        CalculationExercise,
        MaxExercise,
        CoordinatesExercise,
        SumOfPowersExercise,
        RobberyExercise,
        MaxSubArrayExercise,
        FloodExercise],
})
export class AppModule { }
