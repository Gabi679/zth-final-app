import { Controller, HttpService } from "@nestjs/common";
import { Exercise } from "src/decorators/Exercise";
import { BaseExercise } from "src/interfaces/BaseExercise";
import axios, { AxiosResponse } from 'axios';
import { getRandomInt } from "src/utils/RandomNumber";


@Controller()
export class CalculationExercise implements BaseExercise {

    constructor() {

    }

    @Exercise("Calculation")
    mainExercise(url: string, callback) {
        console.log("start Calculation", url);
        let numbersCount = getRandomInt(6) + 1;
        const numbers = [];
        const argumentsList = [];
        while (numbersCount > 0) {
            numbers.push(getRandomInt(10000) - 1);
            numbersCount--;
        }
        for (let index = 0; index < numbers.length - 1; index++) {
            let randomArgument = '';

            switch (getRandomInt(4)) {
                case 1:
                    randomArgument = '+'
                    break;
                case 2:
                    randomArgument = '-'
                    break;
                case 3:
                    randomArgument = '/'
                    break;
                case 4:
                    randomArgument = '*'
                    break;

                default:
                    randomArgument = '+'
                    break;
            }
            argumentsList.push(randomArgument);
        }

        let resultString = '';
        while (argumentsList.length || numbers.length) {
            resultString += numbers.pop();
            if (argumentsList.length) {
                resultString += " " + argumentsList.pop() + " ";
            }
        }
        axios.post(url + "/calculation", {
            mathExercise: resultString
        }, { timeout: 1000 }).then((response: AxiosResponse) => {
            if (Math.round(Number(response.data)) == Math.round(eval(resultString))) {
                console.log("good answer: " + url + "/calculation :" + response.data);
                return callback(1);
            }
            console.log("bad answer: " + url + "/calculation :" + response.data);
            return callback(-1);
        }).catch(error => {
            // console.error(error)
            console.log("no answer" + url + "/calculation");
            callback(0);
        })
    }

}