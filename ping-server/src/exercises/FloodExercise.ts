import { Controller, HttpServer, HttpService } from "@nestjs/common";
import { Exercise } from "src/decorators/Exercise";
import { BaseExercise } from "src/interfaces/BaseExercise";
import axios, { AxiosResponse } from 'axios';
import { getRandomInt } from "src/utils/RandomNumber";

@Controller()
export class FloodExercise implements BaseExercise {

    constructor() {
    }

    @Exercise("Flood")
    mainExercise(url: string, callback) {
        let floodMap = [];
        for (let index = 0; index < 24; index++) {
            floodMap.push(getRandomInt(100))
        }
        if(Math.random()<0.7){
            const randomStartingPoint = getRandomInt(10);
            let randomEndPoint = getRandomInt(10)+randomStartingPoint;
            if(randomEndPoint-randomStartingPoint<5){
                randomEndPoint = randomStartingPoint+5;
            }
            for (let index = randomStartingPoint; index < randomEndPoint; index++) {
                floodMap[index+1] = this.generateBigger(floodMap[index]);
            }
        }
        let floodMapDiffMask = []
        for (let index = 0; index < floodMap.length - 1; index++) {
            floodMapDiffMask.push(floodMap[index + 1] - floodMap[index] > 2 ? 1 : 0);
        }
        let floodBegin = -1000;
        let floodEnd = -1000;
        let count = 0;
        let sequenceFound = false;
        let index = floodMapDiffMask.length - 1;
        while(index >= 0 && (floodBegin === -1000 || floodEnd === -1000)) {
            if(floodMapDiffMask[index] === 1 && !sequenceFound) {
                count++;
                floodEnd = index;
                sequenceFound = true;
            } else if (floodMapDiffMask[index] === 1 && sequenceFound) {
                count++;
            } else if (floodMapDiffMask[index] === 0 && sequenceFound) {
                sequenceFound = false;
                if (count > 3) {
                    floodBegin = index;
                } else {
                    floodEnd = -1000;
                }
                count = 0;
            }
            index--;
        }
        if(floodEnd !== -1000) {
            floodEnd++;
        }
        if(floodBegin !== -1000) {
            floodBegin = floodBegin + 2;
        }
        axios.post(url + "/flood", {
            floodData: floodMap
        }, { timeout: 1000 }).then((response: AxiosResponse) => {
            if (Number(response.data[0]) === floodBegin && Number(response.data[1]) === floodEnd) {
                console.log("good answer: " + url + "/flood :" + response.data);
                return callback(2);
            }
            console.log("bad answer: " + url + "/flood :" + response.data);
            return callback(-1);
        }).catch(error => {
            // console.error(error)
            console.log("no answer" + url + "/flood");
            callback(0);
        })
    }
    generateBigger(number: number): any {
        let generatedNumber = -1;
        let tries = 0;
        while(number >= generatedNumber && tries <=10){
            generatedNumber = getRandomInt(100)+5;
            tries++;
        }
        if(tries >=10){
            return number + 5;
        }
        return generatedNumber+5;
    }
}
