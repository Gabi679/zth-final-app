import { Controller, HttpService } from "@nestjs/common";
import { Exercise } from "src/decorators/Exercise";
import { BaseExercise } from "src/interfaces/BaseExercise";
import axios, { AxiosResponse } from 'axios';
import { getRandomInt } from "src/utils/RandomNumber";
import { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } from "constants";


@Controller()
export class MaxSubArrayExercise implements BaseExercise {

    constructor() {

    }

    @Exercise("MaxSubArray")
    mainExercise(url: string, callback) {

        console.log("start  MaxSubArray", url);
        let generatedInputArray : number[] = this.inputProducer();
        let computedResult : number[] = this.maxSubArraySolver(generatedInputArray);
        axios.post(url + "/maxSubArray", {
            inputArray: generatedInputArray 

        }, { timeout: 1000 }).then((response: AxiosResponse) => {
            let solvedResult : number[] = response.data
            
            if(solvedResult.length > 2) {
                console.log("Result list is too long!");
                callback(-1);
            }

            let isCorrectAnswear : boolean = this.checkCorrectness(computedResult, solvedResult);

            if(isCorrectAnswear) {
                console.log("good answer: " + url + "/maxSubArray :" + response.data);
                return callback(2);
            }

            console.log("bad answer: " + url + "/maxSubArray :" + response.data);
            return callback(-1);
        
        }).catch(error => {
            // console.error(error)
            console.log("no answer" + url + "/maxSubArray");
            callback(0);
        })
    }

    maxSubArraySolver(inputArray: number[] ) : number[] {
        let maxContigSum = 0;
        let maxNonContigSum = 0;
        let currentContigSum = 0;
        let largestValue = 0;

        let resultArray : number[] = [];

        inputArray.forEach(element => {
            // get the maxContigSum
            let value = currentContigSum + element;

            if(value > 0) {
                currentContigSum = value;
            } else {
                currentContigSum = 0;
            }
            //set if grater then the current max
            if(currentContigSum > maxContigSum) {
                maxContigSum = currentContigSum;
            }
            //get the max nonContingSum
            if(element > 0) {
                maxNonContigSum += element;
            }
            // keep largest value
            if(!largestValue || element > largestValue) {
                largestValue = element
            }
        });

        if(maxNonContigSum !== 0) {
            resultArray.push(maxContigSum, maxNonContigSum);
        } else {
            resultArray.push(largestValue, largestValue);
        }

        return resultArray;
    }

    inputProducer() : number[] {
        let upperLimit : number = Math.pow(10, 2)
        let generatedInput : number[] = [];
        let inputsNo : number = getRandomInt(Math.pow(10, 3));
        let chancheForNegativeNumber : number = 1;
        
        for (let index = 0; index < inputsNo; index++) {
            chancheForNegativeNumber = getRandomInt(2);
            
            if(chancheForNegativeNumber === 2) {
                generatedInput[index] = getRandomInt(upperLimit) * -1;
                continue;
            }
            
            generatedInput[index] = getRandomInt(upperLimit);
        }

        return generatedInput;
    }

    checkCorrectness(computedResult: number[], solvedResult: number[] ) : boolean {
        let isCorrect : boolean = false;

        for (let index = 0; index < computedResult.length; index++) {
            if(computedResult[index] === solvedResult[index]){
                isCorrect = true;
            } else {
                return false;
            }
        }

        return isCorrect;
    }

}