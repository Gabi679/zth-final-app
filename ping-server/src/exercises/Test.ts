import { Controller, HttpServer, HttpService } from "@nestjs/common";
import { Exercise } from "src/decorators/Exercise";
import { BaseExercise } from "src/interfaces/BaseExercise";
import axios, { AxiosResponse } from 'axios';

@Controller()
export class TestExercise implements BaseExercise {

    constructor() {

    }

    @Exercise("test")
    mainExercise(url: string, callback) {
        console.log("start test", url);
        axios.get(url + "/ping", { timeout: 1000 }).then((response: AxiosResponse) => {
            if (response.data === 'pong') {
                return callback(true);
            }
            return callback(false);
        }).catch(error => {
            console.log(error)
            callback(false);
        })
    }
}