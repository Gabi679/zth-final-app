import { Controller, HttpServer, HttpService } from "@nestjs/common";
import { Exercise } from "src/decorators/Exercise";
import { BaseExercise } from "src/interfaces/BaseExercise";
import axios, { AxiosResponse } from 'axios';
import localStorage from "../utils/LocalStorage";
import { getRandomInt } from "src/utils/RandomNumber";

@Controller()
export class RobberyExercise implements BaseExercise {

    constructor() {
    }

    @Exercise("Robbery")
    mainExercise(url: string, callback) {
        console.log("start Robbery", url);
        const graph = this.createGraph(new GraphNode(getRandomInt(100)), getRandomInt(3)+2);

        const houses = this.getArray([graph]);
        const res = this.solve(houses);
        axios.post(url + "/robbery", {
            houses
        }, { timeout: 1000 }).then((response: AxiosResponse) => {
            if (Number(response.data) === res) {
                let goodAnswers = Number(localStorage.getItem(url));
                if (goodAnswers) {
                    if (goodAnswers === 10) {
                        goodAnswers++;
                        localStorage.setItem(url, goodAnswers);
                        return callback(1000);
                    } else if (goodAnswers >= 10) {
                        return callback(0);
                    } else {
                        goodAnswers++;
                        localStorage.setItem(url, goodAnswers);
                    }
                } else {
                    localStorage.setItem(url, 1);
                }
                console.log("good answer: " + url + "/robbery :" + response.data);
                return callback(0);
            } else {
                if (Number(localStorage.getItem(url)) !== 11) {
                    localStorage.setItem(url, 0);
                    console.log("bad answer: " + url + "/robbery :" + response.data);
                    return callback(-1);
                }
                console.log("bad answer after 10 good answer: " + url + "/robbery :" + response.data);
                return callback(0);


            }
        }).catch(error => {
            if (Number(localStorage.getItem(url)) !== 11) {
                localStorage.setItem(url, 0);
            }
            console.error(error)
            console.log("no answer" + url + "/lego");
            callback(0);
        })
    }


    getArray(nodes: GraphNode[], array = []) {
        const currentNode = nodes.pop();
        array.push(currentNode ? currentNode.value : null);
        if (currentNode && currentNode.value) {
          nodes = [currentNode.right, currentNode.left, ...nodes];
        }
        if (nodes.length) {
          this.getArray(nodes, array);
        }
        return array;
      }

      solve(houses) {
        let nullCounter = 0;
        let deep = 0;
        const root = this.insertLevelOrder(houses, new GraphNode(houses[0]), 0);
        let max = this.calculate(root);
        if (houses[1]) {
          const leftMax = this.calculate(root.left);
    
          if (leftMax > max) {
            max = leftMax;
          }
        }
        if (houses[2]) {
          const rightMax = this.calculate(root.right);
    
          if (rightMax > max) {
            max = rightMax;
          }
        }
        return max;
      }
    
      calculate(root, money = 0) {
        money += root.value;
        let maxMoney = 0;
        if (root.left && root.left.value && root.left.left && root.left.left.value) {
          let currentMoney = this.calculate(root.left.left);
          currentMoney > maxMoney ? maxMoney = currentMoney : null;
        }
        if (root.left && root.left.right && root.left.value && root.left.right.value) {
          let currentMoney = this.calculate(root.left.right);
          currentMoney > maxMoney ? maxMoney = currentMoney : null;
        }
        if (root.right && root.right.right && root.right.value && root.right.right.value) {
          let currentMoney = this.calculate(root.right.right);
          currentMoney > maxMoney ? maxMoney = currentMoney : null;
        }
        if (root.right && root.right.left && root.right.value && root.right.left.value) {
          let currentMoney = this.calculate(root.right.left);
          currentMoney > maxMoney ? maxMoney = currentMoney : null;
        }
        return money + maxMoney;
      }

    insertLevelOrder(arr: number[], root: GraphNode, i: number): GraphNode {
        // Base case for recursion
        if (i < arr.length) {
            const temp: GraphNode = new GraphNode(arr[i]);
            root = temp;

            // insert left child
            root.left = this.insertLevelOrder(arr, root.left,
                2 * i + 1);

            // insert right child
            root.right = this.insertLevelOrder(arr, root.right,
                2 * i + 2);
        }
        return root;
    }


    createGraph(root: GraphNode, max: number) {
        if(root && root.value!==null && max){
          root.left = this.createGraph(new GraphNode(getRandomInt(100)), max-1)
          root.right = this.createGraph(new GraphNode(getRandomInt(100)), max-1)
        }
        return root;
      }

}

class GraphNode {
    value = null;
    left = null;
    right = null;

    constructor(value) {
        this.value = value;
    }
}

