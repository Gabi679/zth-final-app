import { Controller, HttpServer, HttpService } from "@nestjs/common";
import { Exercise } from "src/decorators/Exercise";
import { BaseExercise } from "src/interfaces/BaseExercise";
import axios, { AxiosResponse } from 'axios';
import { getRandomInt } from "src/utils/RandomNumber";

@Controller()
export class MaxExercise implements BaseExercise {

    constructor() {
    }

    @Exercise("Max")
    mainExercise(url: string, callback) {
        console.log("start MaxExercise", url);
        let numbersCount = getRandomInt(20) + 1;
        const numbers: number[] = [];
        const argumentsList = [];
        while (numbersCount > 0) {
            numbers.push(getRandomInt(1000000) - 1);
            numbersCount--;
        }
        let isMax = true;
        if (getRandomInt(2) === 1) {
            isMax = false;
        }
        const res = isMax ? Math.max(...numbers) : Math.min(...numbers);
        const finalString = `Mennyi az alábbi számok ${isMax ? 'maximuma ' : 'minimuma '} ? ` + numbers.join(',');
        axios.post(url + "/maxOrMin", {
            maxOrMin: finalString
        }, { timeout: 1000 }).then((response: AxiosResponse) => {
            if (Number(response.data) === res) {
                console.log("good answer: " + url + "/maxOrMin :" + response.data);
                return callback(1);
            }
            console.log("bad answer: " + url + "/maxOrMin :" + response.data);
            return callback(-1);
        }).catch(error => {
            // console.error(error)
            console.log("no answer" + url + "/maxOrMin");
            callback(0);
        })
    }




}