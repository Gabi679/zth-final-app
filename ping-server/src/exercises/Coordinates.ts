import { Controller, HttpServer, HttpService } from "@nestjs/common";
import { Exercise } from "src/decorators/Exercise";
import { BaseExercise } from "src/interfaces/BaseExercise";
import axios, { AxiosResponse } from 'axios';
import { getRandomInt } from "src/utils/RandomNumber";

@Controller()
export class CoordinatesExercise implements BaseExercise {

    constructor() {
    }

    @Exercise("Coordinates")
    mainExercise(url: string, callback) {
        console.log("start Coordinates", url);
        const a1 = getRandomInt(1000) + 1;
        const a2 = getRandomInt(1000) + 1;
        const b1 = getRandomInt(1000) + 1;
        const b2 = getRandomInt(1000) + 1;
        const res = Math.sqrt(Math.pow(b1-a1,2)+Math.pow(b2-a2,2))
        axios.post(url + "/coordinates", {
            coordinates: `Point1: [${a1}, ${a2}], Point2: [${b1}, ${b2}]`
        }, { timeout: 1000 }).then((response: AxiosResponse) => {
            if (Math.round(Number(response.data)) === Math.round(res)) {
                console.log("good answer: " + url + "/coordinates :" + response.data);
                return callback(1);
            }
            console.log("bad answer: " + url + "/coordinates :" + response.data);
            return callback(-1);
        }).catch(error => {
            // console.error(error)
            console.log("no answer" + url + "/coordinates");
            callback(0);
        })
    }




}