import { Controller, HttpServer, HttpService } from "@nestjs/common";
import { Exercise } from "src/decorators/Exercise";
import { BaseExercise } from "src/interfaces/BaseExercise";
import axios, { AxiosResponse } from 'axios';
import { getRandomInt } from "src/utils/RandomNumber";

@Controller()
export class ColorMixingExercise implements BaseExercise {

    constructor() {
    }

    @Exercise("ColorMixing")
    mainExercise(url: string, callback) {
        console.log("start ColorMixingExercise ", url);
        const colors = this.getColors();
        axios.post(url + "/colorMixing", {
            colors: colors.colors
        }, { timeout: 1000 }).then((response: AxiosResponse) => {
            if (response.data.toLowerCase() === colors.answer.toLowerCase()) {
                console.log("good answer: " + url + "/colorMixing :" + response.data);
                return callback(1);
            }
            console.log("bad answer: " + url + "/colorMixing :" + response.data);
            return callback(-1);
        }).catch(error => {
            // console.error(error)
            console.log("no answer" + url + "/colorMixing");
            callback(0);
        })
    }

    private getColors() {
        let colorsAmount = getRandomInt(3);
        const colors = [];
        const isAdditive = getRandomInt(2);
        while (colorsAmount > 0) {
            let color;
            if(isAdditive===1){
                const number = getRandomInt(3);
                color = this.getColor(number);
            }else{
                const number = getRandomInt(3)+3;
                color = this.getColor(number);
            }
            if (!colors.includes(color)) {
                colors.push(color);
            }
            colorsAmount--;
        }
        const answer = this.getAnswer(colors);
        return {
            colors,
            answer
        }
    }



    private getColor(number: Number): string {
        let color = 'Red';
        switch (number) {
            case 1:
                color = 'Red'
                break;
            case 2:
                color = 'Blue'
                break;
            case 3:
                color = 'Green'
                break;
            case 4:
                color = 'Yellow'
                break;
            case 5:
                color = 'Cyan'
                break;
            case 6:
                color = 'Magenta'
                break;

            default:
                color = 'Red'
                break;
        }
        return color;
    }

    private getAnswer(colors: string[]): string {
        if (colors.length === 1) {
            return colors[0];
        } else {
            const red = colors.includes("Red");
            const green = colors.includes("Green");
            const blue = colors.includes("Blue");
            const magenta = colors.includes("Magenta");
            const yellow = colors.includes("Yellow");
            const cyan = colors.includes("Cyan");
            if (red && green && blue) {
                return "White";
            } else if (magenta && yellow && cyan) {
                return "Black";
            } else if (red && blue) {
                return "Magenta";
            } else if (red && green) {
                return "Yellow";
            } else if (blue && green) {
                return "Cyan";
            } else if ((magenta && yellow)) {
                return "Red";
            } else if ((magenta && cyan)) {
                return "Blue";
            } else if ((cyan && yellow)) {
                return "Green";
            } else if (yellow) {
                return "Yellow";
            } else if (red) {
                return "Red";
            } else if (blue) {
                return "Blue";
            } else if (magenta) {
                return "Magenta";
            } else if (cyan) {
                return "Cyan";
            }
            return "White";
        }
    }
}