import { Controller, HttpServer, HttpService } from "@nestjs/common";
import { Exercise } from "src/decorators/Exercise";
import { BaseExercise } from "src/interfaces/BaseExercise";
import axios, { AxiosResponse } from 'axios';
import { getRandomInt } from "src/utils/RandomNumber";

@Controller()
export class SumOfPowersExercise implements BaseExercise {

    constructor() {}

    @Exercise("sumOfPowers")
    mainExercise(url: string, callback) {
        console.log("start sumOfPowers", url);
        const n = getRandomInt(3) + 1;
        const x = getRandomInt(1000) + 1;
        const res = this.combinations(x, n, 1);
        axios.post(url + "/sumOfPowers", {
            n,
            x
        }, { timeout: 1000 }).then((response: AxiosResponse) => {
            if (Number(response.data) === res) {
                console.log("good answer: " + url + "/sumOfPowers :" + response.data);
                return callback(2);
            }
            console.log("bad answer: " + url + "/sumOfPowers :" + response.data);
            return callback(-1);
        }).catch(error => {
            // console.error(error)
            console.log("no answer" + url + "/sumOfPowers");
            callback(0);
        })
    }




    combinations(X, N, c) {
        if (X === 0) {
            return 1;
        }
        var result = 0;
        for (var i = c; Math.pow(i, N) <= X; i++) {
            result += this.combinations(X - Math.pow(i, N), N, i + 1);
        }
        return result;
    }



}
