import { SubscribeMessage, WebSocketGateway, OnGatewayInit, WsResponse, WebSocketServer, OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { Logger, HttpService } from '@nestjs/common';
import { Socket, Server } from 'socket.io';
import { finished } from 'stream';
import { registeredFunctions } from "./decorators/Exercise";
import { Result } from './models/Result';
import { User } from './models/User';
import { AppController } from './app.controller';
import { uuid } from 'uuidv4';
import { UserService } from './services/user/user.service';
import { ExerciseService } from './services/exercise/exercise.service';
import fork = require('child_process');

@WebSocketGateway(3001)
export class AppGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

  public resultList: Result[] = [];
  private resultEmitter;
  constructor(private userService: UserService, private exerciseService: ExerciseService, private httpService: HttpService) {
    this.userService.newUserSubject.subscribe((user: User) => {
      if (user) {
        this.resultList.push({
          id: uuid(),
          exercises: this.exerciseService.getRunningExercises().map(id => {
            return {
              id,
              result: 0
            }
          }),
          user
        })
      }
    });
    this.userService.removeUserSubject.subscribe((user: User) => {
      if (user) {
        this.resultList = this.resultList.filter(result=>result.user.ip!==user.ip);
      }
    })
  }

  @WebSocketServer() wss: Server;

  private logger = new Logger('AppGateway');
  currentInterval = {};

  afterInit(server: any) {
    this.logger.log('initialized');
  }

  handleDisconnect(client: Socket) {
    this.logger.log(`client disconnected ${client.id}`);
  }
  handleConnection(client: Socket, ...args: any[]) {
    this.logger.log(`client connected ${client.id}`);
  }


  @SubscribeMessage('start')
  handleMessage(client: Socket, id: string): void {
    console.log("start: ", id)
    if (!this.currentInterval[id]) {
      this.resultList.forEach(result => {
        result.exercises.push({
          id,
          result: 0
        })
      });
    }
    this.currentInterval[id] = setInterval(this.pingExercise.bind(this, id), 2000);
    this.exerciseService.addRunningExercise(id);
    const process = fork.fork('src/persist.ts');
    process.on('message', (message: { result: string }) => {
    });
    process.send(this.resultList);
    if(this.exerciseService.isThereAnyRunningExercise() && this.resultEmitter){
      clearInterval(this.resultEmitter);
      this.resultEmitter = null;
    }
  }

  @SubscribeMessage('stop')
  stop(client: Socket, id: string): void {
    console.log("stop: ", id)
    clearInterval(this.currentInterval[id]);
    this.exerciseService.pauseExercise(id);
    const process = fork.fork('src/persist.ts');
    process.on('message', (message: { result: string }) => {
    });
    process.send(this.resultList);
    if(!this.exerciseService.isThereAnyRunningExercise() && !this.resultEmitter){
      this.resultEmitter = setInterval(()=>{
        this.wss.emit('result', this.resultList);
      },1000);
    }
  }

  @SubscribeMessage('stopAll')
  stopAll(client: Socket): void {
    console.log("stopAll: ")
    Object.values(this.currentInterval).forEach((value: any)=>{
      clearInterval(value);
    });
    Object.keys(this.currentInterval).forEach((key: any)=>{
      this.exerciseService.pauseExercise(key);
    })

    const process = fork.fork('src/persist.ts');
    process.on('message', (message: { result: string }) => {
    });
    process.send(this.resultList);
    if(!this.resultEmitter){
      this.resultEmitter = setInterval(()=>{
        this.wss.emit('result', this.resultList);
      },1000);
    }
  }

  @SubscribeMessage('test')
  test(client: Socket): void {
    let finished = 0
    const userResult = new Map<string, boolean>();
    const users = this.userService.getUsers();
    users.forEach(user => {
      if (registeredFunctions.get('test')) {
        registeredFunctions.get('test')(user.ip, success => {
          userResult.set(user.ip, success);
          this.wss.emit('testResult', JSON.stringify(Array.from(userResult.entries())))
        });
      }
    });
  }


  pingExercise(exerciseId: string) {
    let finished = 0
    this.resultList.forEach(result => {
      if (registeredFunctions.get(exerciseId)) {
        registeredFunctions.get(exerciseId)(result.user.ip, exerciseResultNumber => {
          result.exercises.find(exercise => exercise.id === exerciseId).result += exerciseResultNumber;
          finished++;
          if (finished === this.resultList.length) {
            this.wss.emit('result', this.resultList);
          }
        });
      }
    });
  }

}
